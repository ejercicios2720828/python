def pila_numeros():
    pila = []
    while True:
        try:
            numero = int(input("Ingresa un número: "))
            if numero == 0:
                break
            pila.append(numero)
        except ValueError:
            print("Error: Debes ingresar un número válido.")
    if pila:
        print("Pila de números ingresados:", pila)
        print("Promedio:", sum(pila) / len(pila))
    else:
        print("La pila está vacía.")

pila_numeros()

