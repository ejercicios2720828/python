def cola_numeros():
    from collections import deque
    cola = deque()
    while True:
        try:
            numero = int(input("Ingresa un número: "))
            if numero == 0:
                break
            cola.append(numero)
        except ValueError:
            print("Error: Debes ingresar un número válido.")
    print(list(cola))
    print("Promedio: ", sum(cola) / len(cola))

cola_numeros()
