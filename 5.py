def pila_letras():
    pila = []

    while True:
        letra = input("Ingresa una letra: ")
        if letra.isalpha():
            pila.append(letra)
        elif letra.isdigit():
            break
        else:
            print("Ingreso erroneo:")

    print(pila)
    print("Vocales: ", sum(1 for letra in pila if letra.lower() in 'aeiou'))
    print("Consonantes: ", sum(1 for letra in pila if letra.lower() not in 'aeiou'))

pila_letras()
