def cadenas_intercaladas():
    cadena1 = input("Ingresa la primera cadena: ")
    cadena2 = input("Ingresa la segunda cadena: ")
    resultado = ''.join(i + j for i, j in zip(cadena1, cadena2))
    print(resultado)

cadenas_intercaladas()
