def cola_letras():
    from collections import deque
    cola = deque()
    while True:
        letra = input("Ingresa una letra: ")
        if letra.isalpha():
            cola.append(letra)
        elif letra.isdigit():
            break
        else:
            print("Ingreso erroneo:")
    print(list(cola))
    print("Vocales: ", sum(1 for letra in cola if letra.lower() in 'aeiou'))
    print("Consonantes: ", sum(1 for letra in cola if letra.lower() not in 'aeiou'))

cola_letras()
