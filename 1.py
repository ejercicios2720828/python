#INVERTIR LA CADENA
# Pedir al usuario que ingrese la secuencia de caracteres
secuencia = input("Ingrese la secuencia de caracteres: ")

# Crear una lista vacía para almacenar los caracteres
caracteres = []

# Guardar cada carácter en la lista
for caracter in secuencia:
    caracteres.append(caracter)

# Invertir la secuencia de caracteres
caracteres_invertidos = caracteres[::-1]

# Mostrar la secuencia invertida
print("Secuencia invertida:")
for caracter in caracteres_invertidos:
    print(caracter, end="")
